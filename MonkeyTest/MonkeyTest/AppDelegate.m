//
//  AppDelegate.m
//  MonkeyTest
//
//  Created by Adam Pritchard on 4/06/2014.
//  Copyright (c) 2014 Adam Pritchard. All rights reserved.
//


#import "AppDelegate.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}

@end
