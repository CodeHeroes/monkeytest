//
//  TableViewController.m
//  MonkeyTest
//
//  Created by Adam Pritchard on 6/06/2014.
//  Copyright (c) 2014 Adam Pritchard. All rights reserved.
//


#import "TableViewController.h"
#import "MonkeyTest.h"


@implementation TableViewController


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.tableView.accessibilityIdentifier = @"TestTable";
    
    MonkeyTest* monkey = [[MonkeyTest alloc] initWithTapCount:1000];
    monkey.shouldLogProgress = YES;
    monkey.shouldLogInteractions = YES;
    [monkey releaseTheMonkey];
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = @"Hello";
    
    return cell;
}

@end
