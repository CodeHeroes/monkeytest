//
//  AppDelegate.h
//  MonkeyTest
//
//  Created by Adam Pritchard on 4/06/2014.
//  Copyright (c) 2014 Adam Pritchard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
