//
//  RootViewController.h
//  MonkeyTest
//
//  Created by Adam Pritchard on 6/06/2014.
//  Copyright (c) 2014 Adam Pritchard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITableViewController

@end
