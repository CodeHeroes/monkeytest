//
//  RootViewController.m
//  MonkeyTest
//
//  Created by Adam Pritchard on 6/06/2014.
//  Copyright (c) 2014 Adam Pritchard. All rights reserved.
//

#import "RootViewController.h"
#import "MonkeyTest.h"


@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    MonkeyTest* monkey = [[MonkeyTest alloc] init];
    monkey.shouldLogProgress = YES;
    monkey.shouldLogInteractions = YES;
    [monkey releaseTheMonkey];
}

@end
