//
//  ViewController.m
//  MonkeyTest
//
//  Created by Adam Pritchard on 4/06/2014.
//  Copyright (c) 2014 Adam Pritchard. All rights reserved.
//


#import "ViewController.h"
#import "MonkeyTest.h"


@interface ViewController () <UIAlertViewDelegate>

@end


@implementation ViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    MonkeyTest* monkey = [[MonkeyTest alloc] init];
    monkey.shouldLogProgress = YES;
    monkey.shouldLogInteractions = YES;
    [monkey releaseTheMonkey];
}

- (IBAction)leftButtonPressed:(id)sender
{
    self.view.backgroundColor = [UIColor redColor];
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Alert" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"One", @"Two", nil];
    [alert show];
}

- (IBAction)middleButtonPressed:(id)sender
{
    self.view.backgroundColor = [UIColor blueColor];
}

- (IBAction)rightButtonPressed:(id)sender
{
    self.view.backgroundColor = [UIColor greenColor];
}



#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Dismissed alert by pressing button at index:%d", buttonIndex);
}

@end
