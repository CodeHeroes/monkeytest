//
//  MonkeyTest.h
//  iOS
//
//  Created by Adam Pritchard on 4/06/2014.
//  Copyright (c) 2014 Brendt Sheen. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface MonkeyTest : NSObject

@property (nonatomic) BOOL shouldLogInteractions;
@property (nonatomic) BOOL shouldLogProgress;

- (instancetype)initWithTapCount:(NSUInteger)tapCount;
- (void)releaseTheMonkey;

@end
