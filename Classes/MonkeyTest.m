//
//  MonkeyTests.m
//  MonkeyTests
//
//  Created by Brendt Sheen on 16/04/2014.
//  Copyright (c) 2014 Code Heroes Pty Ltd. All rights reserved.
//


#import "MonkeyTest.h"
#import <KIF.h>
#import <COSTouchVisualizerWindow.h>
#import <objc/runtime.h>


const NSUInteger kDefaultTapCount = 1000;

const CGFloat kWaitAfterTap = 0.05;
const CGFloat kWaitAfterScroll = 0.1;
const CGFloat kWaitAfterAlert = 0.8;
const CGFloat kWaitAfterNoViews = 1.0;


@interface MonkeyTest() <KIFTestActorDelegate>
{
    NSInteger _tapCount;
}

@end


@implementation MonkeyTest

- (instancetype)initWithTapCount:(NSUInteger)tapCount
{
    self = [super init];
    if (self) {
        _tapCount = tapCount;
        _shouldLogInteractions = NO;
        _shouldLogProgress = NO;
        [self setupTouchVisualizer];
    }
    
    return self;
}

- (instancetype)init
{
    return [self initWithTapCount:kDefaultTapCount];
}

- (void)setupTouchVisualizer
{
    id<UIApplicationDelegate> appDelegate = [[UIApplication sharedApplication] delegate];
    UIViewController* rootViewController = appDelegate.window.rootViewController;
    UIColor* tintColor = appDelegate.window.tintColor;
    appDelegate.window = nil;
    
    COSTouchVisualizerWindow* visualiserWindow = [[COSTouchVisualizerWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Touch Color
    [visualiserWindow setFillColor:[UIColor yellowColor]];
    [visualiserWindow setStrokeColor:[UIColor purpleColor]];
    [visualiserWindow setTouchAlpha:0.4];
    
    // Ripple Color
    [visualiserWindow setRippleFillColor:[UIColor yellowColor]];
    [visualiserWindow setRippleStrokeColor:[UIColor purpleColor]];
    [visualiserWindow setRippleAlpha:0.1];
    
    [visualiserWindow  setRootViewController:rootViewController];
    [visualiserWindow  makeKeyAndVisible];
    
    appDelegate.window = visualiserWindow;
    appDelegate.window.tintColor = tintColor;
    
    // Overriding private instance variable
    [visualiserWindow setValue:@YES forKey:@"active"];
}

- (void)releaseTheMonkey
{
    NSLog(@"Monkey Test: Began");
    
    for (int i = 0; i < _tapCount; i++) {
        [self checkForAlertView];
        [self interactWithRandomView];
        
        if (_shouldLogProgress && i % 50 == 0) {
            NSLog(@"Monkey Test: %.0f%%", (float)i/_tapCount * 100);
        }
    }
    
    NSLog(@"Monkey Test: Completed");
}

- (void)checkForAlertView
{
    // This is undocumented API stuff that is really useful, but will likely change.
    
    Class UIAlertManager = objc_getClass("_UIAlertManager");
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    
    UIAlertView* topMostAlert = [UIAlertManager performSelector:@selector(topMostAlert)];
    
#pragma clang diagnostic pop
    
    if (topMostAlert != nil) {
        int randomButtonIndex = arc4random() % topMostAlert.numberOfButtons;
        [topMostAlert dismissWithClickedButtonIndex:randomButtonIndex animated:YES];
        [tester waitForTimeInterval:kWaitAfterAlert];
    }
}

- (void)interactWithRandomView
{
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    NSArray* allViews = [self getAllSubviews:window];
    allViews = [allViews arrayByAddingObject:window];
        
    int index = arc4random() % [allViews count];
    UIView* view = allViews[index];
    [self interactWithView:view];
}

- (NSArray *)getAllSubviews:(UIView *)view
{
    __block NSMutableArray* allSubviews = [[NSMutableArray alloc] init];
    
    [[view subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        UIView* view = (UIView*)obj;
        [allSubviews addObject:view];
        
        if ([view.subviews count] > 0) {
            [allSubviews addObjectsFromArray:[self getAllSubviews:view]];
        }
    }];
    
    return allSubviews.copy;
}

- (void)interactWithView:(UIView *)view
{
    if ([view isKindOfClass:[UIScrollView class]] && view.accessibilityIdentifier != nil) {  // Scroll View must have an Accessibility Identifier
        [self scrollInView:view];
        if (_shouldLogInteractions) NSLog(@"scrolled: %@", view);
        [tester waitForTimeInterval:kWaitAfterScroll];
    } else {
        [view tap];
        if (_shouldLogInteractions) NSLog(@"tapped: %@", view);
        [tester waitForTimeInterval:kWaitAfterTap];
    }
}

- (void)scrollInView:(UIView*)view
{
    // Random floats between -1.0 and 1.0
    float randomHorizontal = (float)arc4random() / 0xFFFFFFFFu * 2 - 1;
    float randomVertical   = (float)arc4random() / 0xFFFFFFFFu * 2 - 1;
    
    [tester scrollViewWithAccessibilityIdentifier:view.accessibilityIdentifier byFractionOfSizeHorizontal:randomHorizontal vertical:randomVertical];
}



#pragma mark - KIFTestActorDelegate

- (void)failWithException:(NSException *)exception stopTest:(BOOL)stop
{
    // Do nothing for now.
}

- (void)failWithExceptions:(NSArray *)exceptions stopTest:(BOOL)stop
{
    // Do nothing for now.
}

@end
