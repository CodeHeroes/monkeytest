Pod::Spec.new do |s|
  s.name     = "MonkeyTest"
  s.version  = "0.1.0"
  s.summary  = "A simple automated integration test that randomly taps buttons on the screen."
  s.homepage = "http://www.codeheroes.com.au/"
  s.license  = 'MIT'
  s.author   = { "Code Heroes" => "contact@codeheroes.com.au" }
  s.source   = { :git => "https://bitbucket.org/adamcodeheroes/monkeytest.git", :tag => s.version.to_s }

  s.platform              = :ios, '6.0'
  s.ios.deployment_target = '6.0'
  s.requires_arc          = true

  s.source_files  = 'Classes'
  s.frameworks    = 'XCTest'
  s.dependency 'KIF'
  s.dependency 'COSTouchVisualizer'
end
